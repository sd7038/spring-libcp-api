package com.apipatio.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CategoriaVeiculo")
public class CategoriaVeiculo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idCategoriaVeiculo;
	
	@Getter
	@Setter
	private String nomeCategoria;

	public CategoriaVeiculo() {
	}
	
	public CategoriaVeiculo(String nomeCategoria) {
		this.setIdCategoriaVeiculo(idCategoriaVeiculo);
	}

	public Integer getIdCategoriaVeiculo() {
		return this.idCategoriaVeiculo;
	}

	public void setIdCategoriaVeiculo(Integer idCategoriaVeiculo) {
		this.idCategoriaVeiculo = idCategoriaVeiculo;
	}

}
