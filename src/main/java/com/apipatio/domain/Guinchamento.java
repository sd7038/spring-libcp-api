package com.apipatio.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Guinchamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Long idGuinchamento;
	
	@Getter
	@Setter
	private String data;
	
	@Getter
	@Setter
	private float extra;

	@Column(name="Guincho_placa")
	@Getter
	@Setter
	private String guinchoPlaca;

	@Getter
	@Setter
	private float km;

	@Lob
	@Getter
	@Setter
	private String obs;

	@Getter
	@Setter
	private String tipo;

	public Guinchamento() {
	}

	public Guinchamento(String data, float extra, String guinchoPlaca, float km, String obs, String tipo) {
		super();
		this.data = data;
		this.extra = extra;
		this.guinchoPlaca = guinchoPlaca;
		this.km = km;
		this.obs = obs;
		this.tipo = tipo;
	}
	

}
