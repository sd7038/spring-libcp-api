package com.apipatio.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;
import org.springframework.beans.factory.annotation.Autowired;

//import com.apipatio.services.TaxaService;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import ch.qos.logback.core.util.TimeUtil;
import lombok.Getter;
import lombok.Setter;

@Entity
@Embeddable
public class Processo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Long idProcesso;
	
	@Getter
	@Setter
	private String alienacao;
	
	@Getter
	@Setter
	private String assinatura;

	@Getter
	@Setter	
	private String chave;
	
	@Getter
	@Setter
	private String codigoProc;

	@Getter
	@Setter
	private String condutor;
	
	//@Column(name="convenio_idConvenio")
//	@Getter
//	@Setter
//	private Integer convenioIdConvenio;

	@Column(name="data_entrada")
	@Getter
	@Setter
	private String dataEntrada;

	@Column(name="data_saida")
	@Getter
	@Setter
	private String dataSaida;

	@Temporal(TemporalType.TIMESTAMP)
	@Getter
	@Setter
	private Date horarioSolicitacao;

	@Getter
	@Setter
	private String matriculaOficial;

	@Lob
	@Column(name="motivo_apre")
	@Getter
	@Setter
	private String motivoApre;

	@Column(name="NID_Veiculo_idVeiculo")
	@Getter
	@Setter
	private Integer NIDVeiculoIdVeiculo;

	@Column(name="num_auto_apre")
	@Getter
	@Setter
	private String numAutoApre;

	@Lob
	@Getter
	@Setter
	private String obsAlienacao;

	@Getter
	@Setter
	private String observacao;
	
	@Column(name="oficiais_idOficiais")
	@Getter
	@Setter
	private String oficiaisIdOficiais;

	@Getter
	@Setter
	private String origem;

	@Column(name="patio_idPatio")
	@Getter
	@Setter
	private String patioIdPatio;
	
	//@Column(name="pessoa_cpfResponsavel")
	//@Getter
	//@Setter
	//private String pessoaCpfResponsavel;

	@Column(name="proprietario_idProprietario")
	@Getter
	@Setter
	private BigInteger proprietarioIdProprietario;

	@Getter
	@Setter
	private String restricao;
	
	@Getter
	@Setter
	private String situacao;

	@Getter
	@Setter
	private String statusAutorizacao;

	@Getter
	@Setter
	private String statusProcesso;

	@Getter
	@Setter
	private String statusValidacao;

	@Getter
	@Setter
	private String statusVeiculo;

	@Column(name="vaga_idVaga")
	@Getter
	@Setter
	private Integer vagaIdVaga;

//	@Column(name="veiculo_idVeiculo")
//	@Getter
//	@Setter
//	private BigInteger veiculoIdVeiculo;
	
	@ManyToOne
	@JoinColumn(name="veiculo_idVeiculo")
	@Getter
	@Setter
	private Veiculo veiculo;
	
 	@ManyToOne
    @JoinColumn(name="convenio_idConvenio")
 	@Getter
 	@Setter
 	private Convenio convenio;
	
	@ManyToOne
    @JoinColumn(name="Pessoa_cpfResponsavel")
	@Getter
	@Setter
	private Pessoa responsavel;
	

	@ManyToMany(cascade = { CascadeType.ALL })
	    @JoinTable(
	        name = "Processo_has_Guinchamento", 
	        joinColumns = { @JoinColumn(name = "Processo_idProcesso") }, 
	        inverseJoinColumns = { @JoinColumn(name = "Guinchamento_idGuinchamento") }
	    )
	@Getter
	@Setter
	Set<Guinchamento> guinchamentos = new HashSet<Guinchamento>();
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "Processo_has_Infracao", 
        joinColumns = { @JoinColumn(name = "Processo_numProc") }, 
        inverseJoinColumns = { @JoinColumn(name = "Infracao_idInfracao") }
    )
	@Getter
	@Setter
	Set<Infracao> infracoes = new HashSet<Infracao>();
	
	public Processo() {
	}
	
	public Processo(String statusProcesso, String dataEntrada) {
		this.setStatusProcesso(statusProcesso);
		this.setDataEntrada(dataEntrada);
	}
	
	@Transient
	public Float getDiarias() {
		Float t = this.convenio.getTaxaDiaria() * this.getDias();
		return t;
	}
	
	@Transient
	public Float getGuinchamento(){
		// a variavel r equivale a remoção
		Guinchamento r = this.guinchamentos.iterator().next();
		Convenio c = this.convenio;
		return (c.getTaxaRemocao() + (r.getKm() * c.getTaxaKM()) + (r.getExtra()));
	}


	@Transient
	public long getDias(){
		//return 878;
		Date de = null;
		Date ds = null;
		try {
			de =  new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.dataEntrada);
			if(Strings.isNullOrEmpty(this.dataSaida)) {
				ds = new Date();
			}
			else {
				ds =  new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.dataSaida);
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long diffInMillies =  ds.getTime() - de.getTime();
	    return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
}