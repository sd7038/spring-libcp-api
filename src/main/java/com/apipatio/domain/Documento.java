package com.apipatio.domain;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="Documentos")
public class Documento implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Integer idDocumentos;

	@Getter
	@Setter
	private Integer altura;

	@Getter
	@Setter
	private Integer auto;

	@Getter
	@Setter
	private String caminho;


	@Getter
	@Setter
	private String data;

	@Getter
	@Setter
	private String extencao;

	@Getter
	@Setter
	private Integer idUsuario;

	@Getter
	@Setter
	private String label;

	@Getter
	@Setter
	private Integer largura;

	@Getter
	@Setter
	private String nomeArquivo;

	@Getter
	@Setter
	private Long numProc;

	@Getter
	@Setter
	private String tipo;

	public Documento() {
	}
	
	public Documento(String caminho, String data, String extencao, String label, String nomeArquivo, Long numProc,
			String tipo) {
		super();
		this.caminho = caminho;
		this.data = data;
		this.extencao = extencao;
		this.label = label;
		this.nomeArquivo = nomeArquivo;
		this.numProc = numProc;
		this.tipo = tipo;
	}


	@JsonProperty("path")
	public String getPath() {
		if(this.caminho.equals("cp_anapolis3")) {
			return "upload/cpanapolis3/" + numProc + "/" + nomeArquivo + extencao;
		}
		if(this.tipo.equals("pdf")) {
			return "upload/" + numProc + "/documentos/" + nomeArquivo + "." + extencao;
		}
		return "upload/" + numProc + "/fotos/" + nomeArquivo + "." + extencao;
	
	}

}
