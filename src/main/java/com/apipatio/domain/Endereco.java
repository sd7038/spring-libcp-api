package com.apipatio.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Endereco implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Long idEndereco;
	
	@Getter
	@Setter
	private String bairro;
	
	@Getter
	@Setter
	private Integer cep;
	
	@Getter
	@Setter
	private String complemento;
	
	@Getter
	@Setter
	private String endereco;
	
	@Getter
	@Setter
	private Integer municipio_idMunicipio;
	
	public Endereco() {
		
	}

	public Endereco(String bairro, Integer cep, String complemento, String endereco, Integer municipio_idMunicipio) {
		super();
		this.bairro = bairro;
		this.cep = cep;
		this.complemento = complemento;
		this.endereco = endereco;
		this.municipio_idMunicipio = municipio_idMunicipio;
	}
	
}
