package com.apipatio.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the veiculo database table.
 * 
 */
@Entity
public class Veiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Long idVeiculo;
	
	@Column(name="ano_fabri")
	@Getter 
	@Setter
	private Integer anoFabri;

	@Column(name="ano_modelo")
	@Getter 
	@Setter
	private Integer anoModelo;

	@Getter 
	@Setter
	private String categoria;

//	@Column(name="categoriaVeiculo_idCategoriaVeiculo")
//	@Getter 
//	@Setter
//	private Integer categoriaVeiculoIdCategoriaVeiculo;

	@Getter 
	@Setter
	private String chassi;

	@Column(name="cidade_regi")
	@Getter 
	@Setter
	private String cidadeRegi;

	@Getter 
	@Setter
	private String combustivel;

//	@Column(name="cor_idCor")
//	@Getter 
//	@Setter
//	private Integer corIdCor;

	@Getter 
	@Setter
	private String crlv;
	
	@Getter 
	@Setter
	private Integer especie;

	@Column(name="modelo_idModelo")
	@Getter 
	@Setter
	private Integer modeloIdModelo;

	@Column(name="num_motor")
	@Getter 
	@Setter
	private String numMotor;

	@Getter 
	@Setter
	private String placa;

	@Getter 
	@Setter
	private String renavam;

	@Getter 
	@Setter
	private Integer tipo;
	
	@ManyToOne
	@JoinColumn(name="cor_idCor")
	@Getter
	@Setter
	private Cor cor;
	
	@ManyToOne
	@JoinColumn(name="categoriaVeiculo_idCategoriaVeiculo")
	@Getter
	@Setter
	private CategoriaVeiculo categoriaVeiculo;

	public Veiculo() {
	}
	
	public Veiculo(String placa, Cor cor, Integer modelo, CategoriaVeiculo categoriaVeiculo) {
		this.setPlaca(placa);
		this.setCor(cor);
		this.setModeloIdModelo(modelo);
		this.setCategoriaVeiculo(categoriaVeiculo);
	}

}