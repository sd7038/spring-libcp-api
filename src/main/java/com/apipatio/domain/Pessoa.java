package com.apipatio.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Pessoa  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Getter
	@Setter
	private String cpf;
	
	@Getter
	@Setter
	private String categoria;
	
	@Getter
	@Setter
	private BigInteger cnh;

	@Column(name="data_nc")
	@Getter
	@Setter
	private String dataNc;

	@Getter
	@Setter
	private String email;

	@Getter
	@Setter
	@Column(name="estado_civil")
	private String estadoCivil;

	@Getter
	@Setter
	private String nome;

	@Lob
	@Column(name="old_id")
	@Getter
	@Setter
	private String oldId;

	@Getter
	@Setter
	private String orgaoRG;

	@Getter
	@Setter
	private String rg;

	@Getter
	@Setter
	private String sexo;

	@Getter
	@Setter
	private String telefone;

	@Column(name="validade_cnh")
	@Getter
	@Setter
	private String validadeCnh;
	
	@ManyToOne
	@JoinColumn(name="endereco_responsavel")
	@Getter
	@Setter
	private Endereco endereco;

	public Pessoa() {
	}

	public Pessoa(String cpf, String dataNc, Endereco endereco, String sexo, String nome) {
		super();
		this.cpf = cpf;
		this.dataNc = dataNc;
		this.sexo = sexo;
		this.nome = nome;
		this.endereco = endereco;
	}

}
