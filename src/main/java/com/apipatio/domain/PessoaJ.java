package com.apipatio.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class PessoaJ implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private String cnpj;
	
	private String cpfResponsavel;
	
	private String email;
	
	private Long endereco_responsavel;
	
	private String nomeFantasia;
	
	private String razaoSocial;
	
	private String telefone;

}
