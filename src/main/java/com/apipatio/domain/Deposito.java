package com.apipatio.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Deposito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private int idDeposito;
	
	@Column(name="contabancaria_idContabancaria")
	@Getter
	@Setter
	private Integer contabancariaIdContabancaria;

	@Temporal(TemporalType.TIMESTAMP)
	@Getter
	@Setter
	private Date dataDeposito;

	//@Column(name="pagamento_idPagamento")
	//@Getter
	//@Setter
	//private int pagamentoIdPagamento;
	
	@OneToOne
	@JoinColumn(name="pagamento_idPagamento")
	@Getter
	@Setter
	@JsonIgnore
	private Pagamento pagamento;

	public Deposito() {
	}
}
