package com.apipatio.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Convenio implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Integer idConvenio;
	
	@Getter
	@Setter
	private Integer categoriaVeiculo_idCategoriaVeiculo;

	@Getter
	@Setter
	private Integer municipio_idMunicipio;

	@Getter
	@Setter
	private Integer orgao_idOrgao;

	
	//private int categoriaVeiculo_idCategoriaVeiculo;

	//private int municipio_idMunicipio;

	//private int orgao_idOrgao;

	@Column(name="patio_idPatio")
	@Getter
	@Setter
	private String patioIdPatio;

	@Getter
	@Setter
	private float percentualGuincheiro;

	@Getter
	@Setter
	private String status;

	@Getter
	@Setter
	private float taxaDiaria;

	@Getter
	@Setter
	private float taxaKM;

	@Getter
	@Setter
	private float taxaRemocao;	
	
	//private CategoriaVeiculo categoriaVeiculo;
	
	public Convenio() {
	}

	public Convenio(Integer categoriaVeiculo_idCategoriaVeiculo, Integer municipio_idMunicipio, Integer orgao_idOrgao,
			String patioIdPatio, float percentualGuincheiro, String status, float taxaDiaria, float taxaKM,
			float taxaRemocao) {
		super();
		this.categoriaVeiculo_idCategoriaVeiculo = categoriaVeiculo_idCategoriaVeiculo;
		this.municipio_idMunicipio = municipio_idMunicipio;
		this.orgao_idOrgao = orgao_idOrgao;
		this.patioIdPatio = patioIdPatio;
		this.percentualGuincheiro = percentualGuincheiro;
		this.status = status;
		this.taxaDiaria = taxaDiaria;
		this.taxaKM = taxaKM;
		this.taxaRemocao = taxaRemocao;
	}
	
}
