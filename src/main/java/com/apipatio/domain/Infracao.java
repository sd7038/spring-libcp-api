package com.apipatio.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Infracao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Integer idInfracao;

	@Getter
	@Setter
	private String artigo;

	@Lob
	@Getter
	@Setter
	private String descricao;

	@Getter
	@Setter
	private String gravidade;
	
	@Getter
	@Setter
	private String nome;

	@Getter
	@Setter
	private String responsavel;

	public Infracao() {
	}

	public Infracao(String artigo, String descricao, String gravidade, String nome, String responsavel) {
		super();
		this.artigo = artigo;
		this.descricao = descricao;
		this.gravidade = gravidade;
		this.nome = nome;
		this.responsavel = responsavel;
	}

}
