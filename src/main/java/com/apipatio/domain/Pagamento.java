package com.apipatio.domain;

import java.io.Serializable;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Pagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter
	private Integer idPagamento;
	
	@Getter
	@Setter
	private String data;


	@Column(name="forma_paga")
	@Getter
	@Setter
	private String formaPaga;

	@Column(name="processo_idProcesso")
	@Getter
	@Setter
	private Long processoIdProcesso;

	@Getter
	@Setter
	private float valor;
	
	@OneToOne(mappedBy="pagamento")
	@Getter
	@Setter
	private Deposito deposito;

	public Pagamento() {
	}

	public Pagamento(String data, String formaPaga, Long processoIdProcesso, float valor) {
		super();
		this.data = data;
		this.formaPaga = formaPaga;
		this.processoIdProcesso = processoIdProcesso;
		this.valor = valor;
		this.deposito = deposito;
	}
	

}
