package com.apipatio.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

public class CategoriaVeiculoDTO  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer idCategoriaVeiculo;
	
	@NotEmpty(message="Preenchimento obrigatório")
	@Length(min=5, max=70, message="O tamanho deve ser entre 5 e 70 caracteres")
	private String nomeCategoria;

	public CategoriaVeiculoDTO(Integer idCategoriaVeiculo, String nomeCategoria) {
		super();
		this.idCategoriaVeiculo = idCategoriaVeiculo;
		this.nomeCategoria = nomeCategoria;
	}

	public Integer getIdCategoriaVeiculo() {
		return idCategoriaVeiculo;
	}

	public void setIdCategoriaVeiculo(Integer idCategoriaVeiculo) {
		this.idCategoriaVeiculo = idCategoriaVeiculo;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}

}
