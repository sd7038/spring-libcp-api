package com.apipatio.resource;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apipatio.domain.Documento;
import com.apipatio.services.DocumentoService;
import com.apipatio.services.ProcessoService;

import java.util.List;

@RestController
@RequestMapping(value="/documentos")
public class DocumentoResource {
	@Autowired
	private DocumentoService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Documento> view(@PathVariable Integer id) {
		Documento obj = this.service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/numProc/{numProc}", method=RequestMethod.GET)
	public ResponseEntity<List<Documento>> documentos(@PathVariable Long numProc) {
		List<Documento> obj = this.service.findAllByNumProc(numProc);
		return ResponseEntity.ok().body(obj);
	}
}
