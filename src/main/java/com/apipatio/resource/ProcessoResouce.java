package com.apipatio.resource;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apipatio.domain.Processo;
import com.apipatio.services.ProcessoService;

@RestController
@RequestMapping(value="/processos")
public class ProcessoResouce {
	@Autowired
	private ProcessoService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Processo> view(@PathVariable Long id) {
		Processo obj = this.service.find(id);
		return ResponseEntity.ok().body(obj);
	}
}
