package com.apipatio.resource;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apipatio.domain.Documento;
import com.apipatio.domain.Pagamento;
import com.apipatio.services.PagamentoService;

@RestController
@RequestMapping(value="/pagamentos")
public class PagamentoResouce {
	@Autowired
	private PagamentoService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Pagamento> view(@PathVariable Integer id) {
		Pagamento obj = this.service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/numProc/{numProc}", method=RequestMethod.GET)
	public ResponseEntity<List<Pagamento>> documentos(@PathVariable Long numProc) {
		List<Pagamento> obj = this.service.findAllByIdProcesso(numProc);
		return ResponseEntity.ok().body(obj);
	}
}
