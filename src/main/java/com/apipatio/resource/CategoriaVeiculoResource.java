package com.apipatio.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.apipatio.domain.CategoriaVeiculo;
import com.apipatio.dto.CategoriaVeiculoDTO;
import com.apipatio.services.CategoriaVeiculoService;

@RestController
@RequestMapping(value="/categoria-veiculos")
public class CategoriaVeiculoResource {
	@Autowired
	private CategoriaVeiculoService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<CategoriaVeiculo> view(@PathVariable Integer id) {
		CategoriaVeiculo obj = this.service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<CategoriaVeiculo>> findAll(){
		List<CategoriaVeiculo> list = this.service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody CategoriaVeiculoDTO objDto) {
		CategoriaVeiculo obj = this.service.fromDto(objDto);
		this.service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(obj.getIdCategoriaVeiculo()).toUri();
		return ResponseEntity.created(uri).build();
	}
}
