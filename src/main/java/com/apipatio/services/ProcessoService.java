package com.apipatio.services;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apipatio.domain.Processo;
import com.apipatio.repositories.ProcessoRepository;

@Service
public class ProcessoService {
	@Autowired 
	private ProcessoRepository repository;
	
	public Processo find(Long id) {
		Processo obj = repository.findById(id).get();
		return obj;
	}
}