package com.apipatio.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apipatio.domain.CategoriaVeiculo;
import com.apipatio.dto.CategoriaVeiculoDTO;
//import com.apipatio.dto.CategoriaVeiculoDTO;
import com.apipatio.repositories.CategoriaVeiculoRepository;

@Service
public class CategoriaVeiculoService {
	@Autowired
	private CategoriaVeiculoRepository repository;
	
	public CategoriaVeiculo find(Integer id) {
		CategoriaVeiculo obj = repository.findById(id).get();
		return obj;
	}
	
	public List<CategoriaVeiculo> findAll(){
		return repository.findAll();
	}
	
	public CategoriaVeiculo fromDto(CategoriaVeiculoDTO dto){
		CategoriaVeiculo obj = new CategoriaVeiculo();
		obj.setIdCategoriaVeiculo(dto.getIdCategoriaVeiculo());
		obj.setNomeCategoria(dto.getNomeCategoria());
		return obj;
	}
	
	public CategoriaVeiculo insert(CategoriaVeiculo obj) {
		obj.setIdCategoriaVeiculo(null);
		return repository.save(obj);
	}
}
