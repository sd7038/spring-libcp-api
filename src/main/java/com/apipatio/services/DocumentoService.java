package com.apipatio.services;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apipatio.domain.Documento;
import com.apipatio.repositories.DocumentoRepository;

@Service
public class DocumentoService {
	@Autowired
	private DocumentoRepository repository;
	
	public Documento find(Integer id) {
		return repository.findById(id).get();
	}
	
	public List<Documento> findAllByNumProc(Long numProc){
		List<Documento> obj = repository.findByNumProc(numProc);
		return obj;
	}

}
