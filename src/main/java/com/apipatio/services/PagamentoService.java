package com.apipatio.services;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apipatio.domain.Pagamento;
import com.apipatio.repositories.PagamentoRepository;

@Service
public class PagamentoService {
	@Autowired
	private PagamentoRepository repository;
	
	public Pagamento find(Integer id) {
		Pagamento obj = repository.findById(id).get();
		return obj;
	}
	
	public List<Pagamento> findAll(){
		return repository.findAll();
	}

	public List<Pagamento> findAllByIdProcesso(Long numProc) {
		List<Pagamento> obj = repository.findByProcessoIdProcesso(numProc);
		return obj;
	}
}
