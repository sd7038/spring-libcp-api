package com.apipatio.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.apipatio.domain.Processo;

@Service
public class TaxaService {
	
	public Map<String, Object> taxa(Processo p){
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("diaria", this.diaria(p));
		res.put("remocao", this.remocao(p));
		return res;
	}
	
	private Map<String, Object> diaria(Processo p){
		return null;
	}
	
	private Map<String, Object> remocao(Processo p){
		return null;
	}
	
	private Map<String, Object> guinchamento(Processo p){
		return null;
	}

}
