package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, String>{

}