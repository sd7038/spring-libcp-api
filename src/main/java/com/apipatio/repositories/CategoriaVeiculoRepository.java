package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apipatio.domain.CategoriaVeiculo;

public interface CategoriaVeiculoRepository extends JpaRepository<CategoriaVeiculo, Integer>{

}
