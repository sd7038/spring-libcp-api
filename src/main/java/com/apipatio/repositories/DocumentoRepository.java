package com.apipatio.repositories;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Documento;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Integer>{
	
	List<Documento> findByNumProc(Long numProc);
}