package com.apipatio.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Processo;

@Repository
public interface ProcessoRepository extends JpaRepository<Processo, Long>{

}