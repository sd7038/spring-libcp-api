package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apipatio.domain.Guinchamento;

public interface GuinchamentoRepository extends JpaRepository<Guinchamento, Long>{


}
