package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Infracao;

@Repository
public interface InfracaoRepository extends JpaRepository<Infracao, Integer>{

}