package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apipatio.domain.Pagamento;

import java.math.BigInteger;
import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Integer>{
	
	List<Pagamento> findByProcessoIdProcesso(Long numProc);

}
