package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Convenio;

@Repository
public interface ConvenioRepository extends JpaRepository<Convenio, Integer>{


}