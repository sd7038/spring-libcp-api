package com.apipatio.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apipatio.domain.Veiculo;

public interface VeiculoRepository extends JpaRepository<Veiculo, BigInteger>{


}