package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apipatio.domain.PessoaJ;

public interface PessoaJRepository<PessoaJRepository> extends JpaRepository<PessoaJ, String> {

}
