package com.apipatio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apipatio.domain.Deposito;

public interface DepositoRepository extends JpaRepository<Deposito, Integer>{

}
